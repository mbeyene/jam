package example;

import static example.ClassWithManyAttributes.MY_DATE;
import static example.ClassWithManyAttributes.MY_NAME;
import static example.ClassWithManyAttributes.MY_NUMBER;

import java.util.Date;

class Example {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ClassWithManyAttributes c = new ClassWithManyAttributes();

		String name = c.getAttributeByName(MY_NAME);
		System.out.println(name);

		Number n = c.getAttributeByName(MY_NUMBER);
		Integer i = (Integer) c.getAttributeByName(MY_NUMBER);

		System.out.println("Number = " + n + " and its " + "subtype Integer = "
				+ i);

		c.setAttributeByName(MY_NUMBER, new Double(5.5));
		n = c.getAttributeByName(MY_NUMBER);
		Double dd = (Double) c.getAttributeByName(MY_NUMBER);

		System.out.println("Number = " + n + " and its " + "subtype Double = "
				+ dd);

		Date d = c.getAttributeByName(MY_DATE);
		System.out.println(d);
	}
}