package example;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import am.component.Attribute;
import am.component.AttributeManager;
import am.component.ComponentFactory;

class ClassWithManyAttributes implements AttributeManager<String> {

	private final static List<Attribute<String, ?>> attributes = new ArrayList<Attribute<String, ?>>();

	public final static Attribute<String, Number> MY_NUMBER = ComponentFactory
			.createAttribute("", Number.class, "The favorite number of the jam programmer.");

	public final static Attribute<String, String> MY_NAME = ComponentFactory
			.createAttribute("name_key", String.class, "The current name of the jam programmer.");

	public final static Attribute<String, Date> MY_DATE = ComponentFactory
			.createAttribute("date_key", Date.class, "A random date that can be used for datish stuff.");

	static {
		attributes.add(MY_NUMBER);
		attributes.add(MY_NAME);
		attributes.add(MY_DATE);
	}

	private final AttributeManager<String> am;

	public ClassWithManyAttributes() {
		Map<Attribute<String, ?>, Object> data = new HashMap<Attribute<String, ?>, Object>();
		initializeFromDB(data);
		this.am = ComponentFactory.createManagerSafe(data);
	}

	@Override
	public <Type, SubType extends Type> void setAttributeByName(
			Attribute<String, Type> attribute, SubType value) {
		am.setAttributeByName(attribute, value);
	}

	@Override
	public <Type> Type getAttributeByName(Attribute<String, Type> attribute) {
		return am.getAttributeByName(attribute);
	}

	private void initializeFromDB(Map<Attribute<String, ?>, Object> data) {
		Map<String, Object> db = generateDB();
		for (Attribute<String, ?> a : attributes) {
			String key = a.getKey();
			Object value = db.get(key);
			if (a.getType().isInstance(value)) {
				data.put(a, a.getType().cast(value));
			}
		}
	}

	private Map<String, Object> generateDB() {
		Map<String, Object> dataBase = new HashMap<String, Object>();
		dataBase.put(MY_DATE.getKey(),
				new GregorianCalendar(2013, 4, 1).getTime());
		dataBase.put(MY_NUMBER.getKey(), new Integer(2000));
		dataBase.put(MY_NAME.getKey(), "MyName");
		return dataBase;
	}
}