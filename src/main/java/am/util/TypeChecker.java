/*
 * Copyright 2014 Mikael Beyene
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package am.util;

/**
 * 
 *
 */
public interface TypeChecker {

	/**
	 * Checks whether <code>value</code> is compatible to type specified in
	 * <code>clazz</code>.
	 * 
	 * If <code>clazz</code> represents a primitive type, <code>value</code>'s
	 * type is compared to its wrapper class.
	 * 
	 * @param clazz
	 *            reference class
	 * @param value
	 *            object whose type is to be checked.
	 * @return true, if <code>value</code> is instance of <code>clazz</code>
	 */
	public boolean checkTypeCompatibility(Class<?> clazz, Object value);
}
