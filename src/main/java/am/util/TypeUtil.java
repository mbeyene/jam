/*
 * Copyright 2014 Mikael Beyene
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package am.util;

import java.util.HashMap;
import java.util.Map;

public final class TypeUtil {

	public static TypeChecker getInstance() {
		return TypeCheckerImpl.INSTANCE;
	}

	private enum TypeCheckerImpl implements TypeChecker {

		INSTANCE;

		private static Map<Class<?>, Class<?>> primitiveWrapperTypeMap;

		static {
			primitiveWrapperTypeMap = new HashMap<Class<?>, Class<?>>();
			primitiveWrapperTypeMap.put(int.class, Integer.class);
			primitiveWrapperTypeMap.put(byte.class, Byte.class);
			primitiveWrapperTypeMap.put(char.class, Character.class);
			primitiveWrapperTypeMap.put(double.class, Double.class);
			primitiveWrapperTypeMap.put(float.class, Float.class);
			primitiveWrapperTypeMap.put(boolean.class, Boolean.class);
			primitiveWrapperTypeMap.put(long.class, Long.class);
			primitiveWrapperTypeMap.put(short.class, Short.class);
			primitiveWrapperTypeMap.put(void.class, Void.class);
		}

		@Override
		public boolean checkTypeCompatibility(Class<?> clazz, Object value) {
			boolean matchingTypes = false;

			if (clazz.isPrimitive()) {
				Class<?> wrapper = primitiveWrapperTypeMap.get(clazz);
				if (wrapper != null && wrapper.isInstance(value))
					matchingTypes = true;
			} else if (clazz.isInstance(value)) {
				matchingTypes = true;
			}
			return matchingTypes;
		}
	}
}