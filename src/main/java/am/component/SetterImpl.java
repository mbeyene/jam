/*
 * Copyright 2014 Mikael Beyene
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package am.component;

import java.util.Map;

/**
 * 
 * @param <Key>
 */
final class SetterImpl<Key> implements Setter<Key> {

	private final Map<Attribute<Key, ?>, Object> data;

	/**
	 * @param data
	 */
	SetterImpl(Map<Attribute<Key, ?>, Object> data) {
		this.data = data;
	}


	@Override
	public <Type, SubType extends Type> void setAttributeByName(
			Attribute<Key, Type> attribute, SubType value) {
		data.put(attribute, attribute.getType().cast(value));
	}
}