/*
 * Copyright 2014 Mikael Beyene
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package am.component;

final class AttributeImpl<Key, Type> implements Attribute<Key, Type> {

	private final Key key;
	private final Class<Type> clazz;
	private final String information;

	private AttributeImpl(Builder<Key, Type> builder) {
		this.key = builder.key;
		this.clazz = builder.clazz;
		this.information = builder.information;
	}

	@Override
	public Class<Type> getType() {
		return clazz;
	}

	@Override
	public Key getKey() {
		return key;
	}

	@Override
	public String getInformation() {
		return information;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((clazz == null) ? 0 : clazz.getCanonicalName().hashCode());
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Attribute))
			return false;
		Attribute<?, ?> other = (Attribute<?, ?>) obj;
		if (clazz == null) {
			if (other.getType() != null)
				return false;
		} else if (!clazz.equals(other.getType()))
			return false;
		if (key == null) {
			if (other.getKey() != null)
				return false;
		} else if (!key.equals(other.getKey()))
			return false;
		return true;
	}

	static class Builder<Key, Type> {
		private Key key;
		private Class<Type> clazz;
		private String information = "";

		AttributeImpl<Key, Type> build() {
			return new AttributeImpl<Key, Type>(this);
		}

		Builder<Key, Type> setKey(Key key) {
			this.key = key;
			return this;
		}

		Builder<Key, Type> setClazz(Class<Type> clazz) {
			this.clazz = clazz;
			return this;
		}

		Builder<Key, Type> setInformation(String information) {
			this.information = information;
			return this;
		}
	}
}