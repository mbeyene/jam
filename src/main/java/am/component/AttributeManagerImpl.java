/*
 * Copyright 2014 Mikael Beyene
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package am.component;

import java.util.Map;

/**
 * 
 * 
 * @param <Key>
 */
class AttributeManagerImpl<Key> implements AttributeManager<Key> {

	private final Setter<Key> setter;
	private final Getter<Key> getter;
	private final NullValueHandler nullHandler;

	private AttributeManagerImpl(Builder<Key> builder) {
		this.setter = builder.setter;
		this.getter = builder.getter;
		this.nullHandler = builder.handler;
	}

	@Override
	public <Type, SubType extends Type> void setAttributeByName(
			Attribute<Key, Type> attribute, SubType value) {
		setter.setAttributeByName(attribute, value);

	}

	@Override
	public <Type> Type getAttributeByName(Attribute<Key, Type> attribute) {
		Type value = getter.getAttributeByName(attribute);
		if (value != null)
			return value;
		else
			return nullHandler.getStandardValue(attribute.getType());
	}

	static class Builder<Key> {
		private Setter<Key> setter;
		private Getter<Key> getter;
		private Map<Attribute<Key, ?>, Object> data;
		private NullValueHandler handler = ComponentFactory.createNullHandler();

		AttributeManagerImpl<Key> build() {
			if (data == null)
				new NullPointerException("Data map may not be null!");

			this.setter = ComponentFactory.createSetter(data);
			this.getter = ComponentFactory.createGetter(data);
			return new AttributeManagerImpl<>(this);
		}

		Builder<Key> setData(Map<Attribute<Key, ?>, Object> data) {
			if (data == null)
				new NullPointerException("Data map may not be null!");

			this.data = data;
			return this;
		}

		Builder<Key> setNullHandler(NullValueHandler handler) {
			if (handler == null)
				new NullPointerException("Handler may not be null!");

			this.handler = handler;
			return this;
		}
	}
}