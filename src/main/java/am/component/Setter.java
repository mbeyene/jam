/*
 * Copyright 2014 Mikael Beyene
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package am.component;

/**
 * Generic setter.
 * 
 * @param <Key>
 *            type of attribute's key
 * 
 * @author mbeyene
 */
public interface Setter<Key> {

	/**
	 * Generic setter.
	 * 
	 * Sets desired class attribute.
	 * 
	 * @param attribute
	 *            key that specifies desired class attribute.
	 * @param value
	 *            value to be set
	 * @throws ClassCastException
	 *             if the object is not null and is not assignable to the type
	 *             T.()}
	 */
	public <Type, SubType extends Type> void setAttributeByName(
			Attribute<Key, Type> attribute, SubType value);

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode();

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object obj);
}