/*
 * Copyright 2014 Mikael Beyene
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package am.component;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import am.component.AttributeManagerImpl.Builder;

/**
 * Factory for attribute manager and attributes.
 * 
 * @author mbeyene
 *
 */
public final class ComponentFactory {

	private ComponentFactory() {
		/* private constructor to preven instantiation of utility class */
	}

	static <Key> Getter<Key> createGetter(Map<Attribute<Key, ?>, Object> data) {
		return new GetterImpl<>(data);
	}

	static <Key> Setter<Key> createSetter(Map<Attribute<Key, ?>, Object> data) {
		return new SetterImpl<>(data);
	}

	static NullValueHandler createNullHandler() {
		return new SimpleNullValueHandler();
	}

	public static <Key> AttributeManager<Key> createManager() {
		return new Builder<Key>().setData(new HashMap<Attribute<Key, ?>, Object>()).build();
	}
	
	public static <Key> AttributeManager<Key> createSynchronizedManager() {
		return new Builder<Key>().setData(new ConcurrentHashMap<Attribute<Key, ?>, Object>()).build();
	}
	
	public static <Key> AttributeManager<Key> createManager(Map<Attribute<Key, ?>, Object> data) {
		return new Builder<Key>().setData(data).build();
	}
	
	public static <Key> AttributeManager<Key> createManager(Map<Attribute<Key, ?>, Object> data, NullValueHandler handler) {
		return new Builder<Key>().setData(data).setNullHandler(handler).build();
	}

	public static <Key> AttributeManager<Key> createManagerSafe(Map<Attribute<Key, ?>, Object> data) {
		/* creates defensive (shallow) copy of map */
		return new Builder<Key>().setData(new HashMap<>(data)).build();
	}

	public static <Key> AttributeManager<Key> createManagerSafe(Map<Attribute<Key, ?>, Object> data, NullValueHandler handler) {
		/* creates defensive (shallow) copy of map */
		return new Builder<Key>().setData(new HashMap<>(data)).setNullHandler(handler).build();
	}

	public static <Key, Type> Attribute<Key, Type> createAttribute(Key key, Class<Type> clazz) {
		return new AttributeImpl.Builder<Key, Type>().setKey(key).setClazz(clazz).build();
	}
	
	public static <Key, Type> Attribute<Key, Type> createAttribute(Key key, Class<Type> clazz, String information) {
		return new AttributeImpl.Builder<Key, Type>().setKey(key).setClazz(clazz).setInformation(information).build();
	}
}