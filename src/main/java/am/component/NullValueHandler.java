/*
 * Copyright 2014 Mikael Beyene
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package am.component;

/**
 * <p>
 * Null value handler for attribute manager.
 * </p>
 * 
 * Specifies behavior of attribute manager if no mapping for key is found.
 * 
 * @author mbeyene
 */
public interface NullValueHandler {

	/**
	 * <p>
	 * Return standard value for given type.
	 * </p>
	 * 
	 * @param clazz
	 *            type that is requested
	 * @return instance of type, that serves as standard value
	 */
	public <T> T getStandardValue(Class<T> clazz);
}
