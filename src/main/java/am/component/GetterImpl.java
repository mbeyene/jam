/*
 * Copyright 2014 Mikael Beyene
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package am.component;

import java.util.Map;

/**
 * 
 * @param <Key>
 */
final class GetterImpl<Key> implements Getter<Key> {

	private final Map<Attribute<Key, ?>, Object> data;

	/**
	 * @param data
	 */
	GetterImpl(Map<Attribute<Key, ?>, Object> data) {
		this.data = data;
	}

	/*
	 * not 100% safe since map is passed to class and we don't not know who else
	 * has references to it
	 */
	@Override
	public <Type> Type getAttributeByName(Attribute<Key, Type> attribute) {
		Type value = attribute.getType().cast(data.get(attribute));
		return value;
	}
}