/*
 * Copyright 2014 Mikael Beyene
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package am.component;

/**
 * Represents a classes attribute.
 * 
 * Represents a classes attribute and information associated with it.
 * Implementing class should be immutable.
 * 
 * 
 * @param <Key>
 *            type of attribute's key
 * 
 * @author mbeyene
 */
public interface Attribute<Key, Type> extends Identifiable<Key>, Typable<Type> {

	/**
	 * Returns information about the attribute.
	 * 
	 * Returns information about the attribute to especially
	 * help non-domain experts.
	 * 
	 * @return information
	 */
	public String getInformation();
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode();

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object obj);
}