package am.component;

import java.util.HashMap;
import java.util.Map;

import junit.framework.Assert;

import org.junit.BeforeClass;
import org.junit.Test;

public class GetterImplTest {

	private static Map<Attribute<String, ?>, Object> mapStr;
	private static Map<Attribute<Integer, ?>, Object> mapInt;

	private static Getter<String> getterStr;
	private static Getter<Integer> getterInt;
	
	private static final Attribute<String, Number> MY_NUM = ComponentFactory.createAttribute("keyForNum", Number.class);
	private static final Attribute<Integer, String> MY_STRING = ComponentFactory.createAttribute(01564, String.class);

	@BeforeClass
	public static void setUp() {
		mapStr = new HashMap<>();
		mapStr.put(MY_NUM, 1234);
		
		mapInt = new HashMap<>();
		mapInt.put(MY_STRING, "test");

		getterStr = ComponentFactory.createGetter(mapStr);
		getterInt = ComponentFactory.createGetter(mapInt);
	}

	@Test
	public void testGetAttributeByName() throws Exception {
		Assert.assertEquals("test", getterInt.getAttributeByName(MY_STRING));
		Assert.assertEquals(1234, getterStr.getAttributeByName(MY_NUM));
	}
}