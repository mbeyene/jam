package am.component;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;

public class SetterImplTest {
	
	private static Map<Attribute<String, ?>, Object> mapStr;
	private static Map<Attribute<Integer, ?>, Object> mapInt;
	
	private static Setter<String> setterStr;
	private static Setter<Integer> setterInt;
	
	private static final Attribute<String, Number> MY_NUM = ComponentFactory.createAttribute("keyForNum", Number.class);
	private static final Attribute<Integer, String> MY_STRING = ComponentFactory.createAttribute(01564, String.class);
	
	@BeforeClass
	public static void setUp() {
		mapStr = new HashMap<>();
		mapInt = new HashMap<>();
		
		setterStr = new SetterImpl<>(mapStr);
		setterInt = new SetterImpl<>(mapInt);
	}

	@Test
	public void testSetAttributeByName() {
		int i = 7;
		setterStr.setAttributeByName(MY_NUM, i);
		assertEquals(i, mapStr.get(MY_NUM));
		
		Double d = new Double(5.5);
		setterStr.setAttributeByName(MY_NUM, d);
		assertEquals(d, mapStr.get(MY_NUM));
		assertTrue(MY_NUM.getType().isInstance(mapStr.get(MY_NUM)));
		
		String s = "blabla";
		setterInt.setAttributeByName(MY_STRING, s);
		assertEquals(s, mapInt.get(MY_STRING));
		
		String t = "someMoreBla";
		setterInt.setAttributeByName(MY_STRING, t);
		assertEquals(t, mapInt.get(MY_STRING));
		assertTrue(MY_STRING.getType().isInstance(mapInt.get(MY_STRING)));
	}
}