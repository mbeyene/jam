package am.component;

import junit.framework.Assert;

import org.junit.Test;


public class AttributeImplTest {

	private static Attribute<String, String> ATTR_STR = ComponentFactory.createAttribute("Bla", String.class, "Some Info");
	private static Attribute<String, String> ATTR_STR_CPY = ComponentFactory.createAttribute("Bla", String.class);
	private static Attribute<String, String> ATTR_STR_OTHER = ComponentFactory.createAttribute("BlaBla", String.class, "Related Info");
	
	@Test
	public void testEquals() throws Exception {
		Assert.assertTrue(ATTR_STR.equals(ATTR_STR_CPY)); // Field 'information' should not be considered in equals()
		Assert.assertTrue(!ATTR_STR.equals(ATTR_STR_OTHER));
	}

	@Test
	public void testHashCode() throws Exception {
		Assert.assertEquals(ATTR_STR.hashCode(), ATTR_STR_CPY.hashCode());
		Assert.assertTrue(ATTR_STR.hashCode() != ATTR_STR_OTHER.hashCode());
	}
}