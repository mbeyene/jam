package am.component;

import java.util.HashMap;
import java.util.Map;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import am.component.AttributeManagerImpl.Builder;

public class AttributeManagerImplTest {

	private static final Attribute<String, Number> MY_NUM = ComponentFactory.createAttribute("keyForNum", Number.class);
	private static final Attribute<String, String> MY_STRING = ComponentFactory.createAttribute("blop", String.class);
	private static final Attribute<String, String> NOT_DEF = ComponentFactory.createAttribute("not-def", String.class);
	private static final Attribute<String, Boolean> SET_ME = ComponentFactory.createAttribute("set-me", Boolean.class);

	private static final Map<Attribute<String, ?>, Object> data = new HashMap<>();
	private static AttributeManager<String> manager;
	private static NullValueHandler handler = new NullValueHandler() {
		@Override
		public <T> T getStandardValue(Class<T> clazz) {
			return null;
		}
	};

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		data.put(MY_NUM, 5);
		data.put(MY_STRING, "words");
	}

	@Before
	public void setUp() throws Exception {
		manager = new Builder<String>().setData(new HashMap<>(data)).setNullHandler(handler).build();
	}

	@Test
	public void testAttributeManagerImpl() throws Exception {
		Assert.assertNull(manager.getAttributeByName(SET_ME));
		manager.setAttributeByName(SET_ME, Boolean.TRUE);
		Assert.assertTrue(manager.getAttributeByName(SET_ME));
	}

	@Test
	public void testGetAttributeByName() throws Exception {
		Assert.assertEquals(5, manager.getAttributeByName(MY_NUM));
		Assert.assertNull(manager.getAttributeByName(NOT_DEF));
	}

}